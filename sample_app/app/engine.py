import logging
import time

logging.basicConfig(level=logging.INFO)
LOG = logging.getLogger("jenkins:test-app")
LOG.setLevel(logging.INFO)


class JenkinsTest():

    def __init__(self):
        
        self.square(4)

        con = True
        while (con):
            time.sleep(1)


    def square(self, x):
        LOG.info("Calculating square of {0}!".format(x))
        result = x * x
        LOG.info("Square of {0} is {1}!".format(x, result))
        return result

def main():
    JenkinsTest()

if __name__ == "__main__":
    main()