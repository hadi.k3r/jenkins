import unittest
from app.engine import JenkinsTest

class TestSquare(unittest.TestCase):
    def test(self):
        self.assertEqual(JenkinsTest.square(self, 3), 9, msg='Failed!')

if __name__ == '__main__':
    unittest.main()